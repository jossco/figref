# README #

LaTeX package to automatically place a figure following the first in-text reference to that figure.

Based on <http://tex.stackexchange.com/a/46587>

## How it works ##

-   code for figure appears in the file `fig/label` and looks like this:

        \begin{figure}[htbp]
          ...
          \caption{caption here}\label{fig:label}
        \end{figure}

-   `label` is both the filename, and the label defined in the file

-   Create references in the text without the `fig:` part, like this:

        \figref{label}

-   The above assumes that the first reference occurs outside any strange environments (i.e., not in a minipage).
    If invoking `\figref` inside another environment, you may need to use `\figref*{}` instead.
    But the starred version does NOT include the `label.tex` file.

-   If you have loaded the `subfig` package, you can pass an optional argument:

        \figref[sublabel]{label}

    Where `label.tex` looks like this

        \begin{figure}[htbp]
          \subfloat[][subcaption here]{\label{subfig:sublabel}
            ...
          }
          ...
          \caption{main caption here}\label{fig:label}
        \end{figure}

-   If the `cleveref` package is loaded *before* `figref`, then the reference will be generated with `\cref{fig:label}`;
    otherwise, it will be `Fig.~\ref{fig:label}`.
    In the former case, `cleveref` is responsible for the formatting of the cross-reference name;
    in the latter case, it is currently fixed to `Fig.`.	